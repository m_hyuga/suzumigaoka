<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86 
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'suzumigaoka-h_wp');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'suzumigaoka-h');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'Mafia001');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql315.db.sakura.ne.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ovOzvGqblZfs;M*l|IE4pZX?Nz&,[rtWEcr#*|-v}zVk>mE+T7m3GcueC61KNroj');
define('SECURE_AUTH_KEY',  'u;.lR1Mp_FC+pi|%/C%dla|!-j>c&8-+k9+=,7@kvdf00]in;zq||a3k%ITy8:L7');
define('LOGGED_IN_KEY',    ',M|P&(grTqK(D[/_&OcPJG+>*|%B|{!2Tn(iNr+NJd|hNlM2B<_O}]SND._ik1lL');
define('NONCE_KEY',        'Emov)p5~M0_jw7/)= Tku@k?m6wtl;+2W 5419-,I9PX(2BKOBRKhIZ|g6H} dGY');
define('AUTH_SALT',        '37Z.>:`m~JLGXA,0a|v=?d3d>Jt|N)mS7O7rA)++^+BwjZ6kg}p8fY%[zp__``T0');
define('SECURE_AUTH_SALT', '+Z0MihmxKR,M;ky;|.U+#IewDf^z0d|B)dIBLeUtl~@gf`_p&#2:iR{Y)!WI^G>!');
define('LOGGED_IN_SALT',   '_okS}[1-|KFMF-f}+-_6YWpa1eCS,F::)^y+a-E8+P-7%LQSoP/}oxb:)[bLu^{T');
define('NONCE_SALT',       '(K8*1_hb$m,rFfJb@u6PNcO)ia)n3EPoO<LEBfqj6(yyG;!0Y>03>Wo`$6,kV,+K');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
