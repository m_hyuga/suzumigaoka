<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="main_image">
<ul class="cf">
<li><a href="<?php bloginfo('url'); ?>">ホーム</a></li>
<li><?php if ( is_post_type_archive('information')) :?>大切なお知らせ<?php else: ?>新着情報<?php endif; ?></li>
</ul>
<h2><img src="<?php bloginfo('template_url'); ?>/common/images/title/<?php if ( is_post_type_archive('information')) :?>img_taisetsu.jpg" alt="大切なお知らせ"<?php else: ?>img_news.jpg" alt="新着情報"<?php endif; ?>></h2>
</div>

<div id="changeArea">
<div id="content" class="content cf">
<?php get_sidebar(); ?>
<section class="flr archive_page">
	<ul class="news_list">
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<li class="cf"><span><?php the_time('Y.m.d'); ?></span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	<?php endwhile; else : ?><li>記事がありません。</li><?php endif; ?>
	</ul>
	
	<?php wp_pagenavi(); ?>
</section>
</div>

<?php
get_footer();
