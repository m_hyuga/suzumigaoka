<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
?>
<!doctype html>
<html lang="ja-JP">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
	<meta charset="UTF-8">
	<meta content="医療法人社団六豊会　すずみが丘病院" name="copyright">
	<meta name="viewport" content="target-densitydpi=device-dpi, width=1000, maximum-scale=1.0, user-scalable=yes">
	<meta name="format-detection" content="telephone=no" />
	<meta name="description" content="石川県金沢市もりの里にあるすずみが丘病院です。内科・神経内科・整形外科・外科・リハビリテーション科の外来診療・入院治療や予防接種・健康診断などを行っています。介護保険の通所リハビリテーション（デイケア）・居宅介護支援事業所を併設しています。">
<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/reset.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/page.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/colorbox/colorbox.css" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/colorbox/jquery.colorbox-min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/top.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/rollover2.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.cookie.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.animate-colors-min.js"></script>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-65903414-2', 'auto');
 ga('require', 'displayfeatures');
 ga('send', 'pageview');

</script>
</head>
<body>
<div class="wrap">
	<header>
		<div id="head_content">
			<p>【金沢市もりの里】内科・整形外科・神経内科・外科・リハビリテーション科 </p>
			<ul id="fontSize">
				<li><img src="<?php bloginfo('template_url'); ?>/common/images/common/txt_changetxt.png" alt="文字の大きさ"></li>
				<li><img src="<?php bloginfo('template_url'); ?>/common/images/common/ico_s.png" alt="小" class="changeBtn"></li>
				<li><img src="<?php bloginfo('template_url'); ?>/common/images/common/ico_m.png" alt="中" class="changeBtn"></li>
				<li><img src="<?php bloginfo('template_url'); ?>/common/images/common/ico_l.png" alt="大" class="changeBtn"></li>
			</ul>
			<div class="search_box">
			<?php
					// 検索フォームを出力する
					get_search_form();
					 
					// 検索フォームを代入する
					$search_form = get_search_form(false); ?>
			</div>
		</div>
		
		<div id="header" class="cf">
			<h1><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/common/images/common/logo.png" alt="医療法人社団六豊会　すずみが丘病院" class="over"></a></h1>
			<ul>
				<li><a href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_contact.png" alt="お問い合わせ" class="over"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/sitemap/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_sitemap.png" alt="サイトマップ" class="over"></a></li>
			</ul>
			<p><img src="<?php bloginfo('template_url'); ?>/common/images/common/img_head_tell.png" alt="お電話でのお問い合わせはこちら TEL.076-260-7700"></p>
		</div>
		<nav>
			<ul>
				<li><a href="<?php bloginfo('url'); ?>/outpatient/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_outpatient_off.png" alt="外来診療のご案内"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/guide/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_guide_off.png" alt="入院のご案内"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/outline/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_outline_off.png" alt="病院のご案内"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/recruitment/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_recruitment_off.png" alt="採用情報"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/nav_contact_off.png" alt="お問い合わせ"></a></li>
			</ul>
		</nav>
	</header>
