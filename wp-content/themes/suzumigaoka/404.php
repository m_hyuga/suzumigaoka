<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="main_image">
<ul class="cf">
<li><a href="<?php bloginfo('url'); ?>">ホーム</a></li>
<li>ページが見つかりません</li>
</ul>
</div>

<div id="changeArea">
<div id="content" class="content cf">
<?php get_sidebar(); ?>
<section class="flr medical_page">
				<p>ページが見つかりませんでした。</p>
				<br><br>
				<p><a href="<?php bloginfo('url'); ?>">トップページへ戻る</a></p>
</section>
</div>
<?php
get_footer();
