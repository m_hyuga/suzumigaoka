<?php
/**
 * The sidebar containing the main widget area */
?>
	<section class="fll">
	<?php if ( is_page('outline') || is_parent_slug() === 'outline' ) { ?>
		<ul class="side_navi">
			<li class="btn_arrow<?php if ( is_page('outline')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/outline/">ごあいさつ</a></li>
			<li class="btn_arrow<?php if ( is_page('doctor')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/outline/doctor/">医師紹介</a></li>
			<li class="btn_arrow<?php if ( is_page('rinen')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/outline/rinen/">基本理念</a></li>
			<li class="btn_arrow<?php if ( is_page('detail')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/outline/detail/">病院概要</a></li>
			<li class="btn_arrow<?php if ( is_page('shisetsu')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/outline/shisetsu/">施設紹介</a></li>
			<li class="btn_arrow<?php if ( is_page('shien')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/outline/shien/">居宅介護支援事業所</a></li>
			<li class="btn_arrow<?php if ( is_page('access')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/outline/access/">アクセス</a></li>
		</ul>
	<?php } ?>
	<?php if ( is_page('guide') || is_parent_slug() === 'guide' ) { ?>
		<ul class="side_navi">
			<li class="btn_arrow<?php if ( is_page('guide')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/guide/">入院のお手続きについて</a></li>
			<li class="btn_arrow<?php if ( is_page('meeting')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/guide/meeting/">お見舞い・面会について</a></li>
		</ul>
	<?php } ?>
	<?php if ( is_page('outpatient') || is_parent_slug() === 'outpatient' || is_parent_slug() === 'medical' ) { ?>
		<ul class="side_navi">
			<li class="btn_arrow<?php if ( is_page('outpatient')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/outpatient/">診療時間・受付について</a></li>
			<li class="btn_arrow<?php if ( is_page('medical')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/outpatient/medical/">診療科目のご紹介</a></li>
			<li<?php if ( is_page('naika')) { echo ' class="on"';} ?>><a href="<?php bloginfo('url'); ?>/outpatient/medical/naika/">内科</a></li>
			<li<?php if ( is_page('seikei')) { echo ' class="on"';} ?>><a href="<?php bloginfo('url'); ?>/outpatient/medical/seikei/">整形外科</a></li>
			<li<?php if ( is_page('shinkei')) { echo ' class="on"';} ?>><a href="<?php bloginfo('url'); ?>/outpatient/medical/shinkei/">神経内科</a></li>
			<li<?php if ( is_page('geka')) { echo ' class="on"';} ?>><a href="<?php bloginfo('url'); ?>/outpatient/medical/geka/">外科</a></li>
			<li<?php if ( is_page('rihabiri')) { echo ' class="on"';} ?>><a href="<?php bloginfo('url'); ?>/outpatient/medical/rihabiri/">リハビリテーション科</a></li>
			<li class="btn_arrow<?php if ( is_page('tsusho')) { echo ' on';} ?>"><a href="<?php bloginfo('url'); ?>/outpatient/tsusho/">通所リハビリテーション</a></li>
		</ul>
	<?php } ?>
	<?php if ( is_page('recruitment')) { ?>
		<ul class="side_navi">
			<li class="btn_arrow on"><a href="<?php bloginfo('url'); ?>/recruitment/">採用情報</a></li>
		</ul>
	<?php } ?>
	<?php if ( is_page('sitemap')) { ?>
		<ul class="side_navi">
			<li class="btn_arrow on"><a href="<?php bloginfo('url'); ?>/sitemap/">サイトマップ</a></li>
		</ul>
	<?php } ?>
	<?php if ( is_page('contact')) { ?>
		<ul class="side_navi">
			<li class="btn_arrow on"><a href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></li>
		</ul>
	<?php } ?>
	<?php if ( is_page('privacy')) { ?>
		<ul class="side_navi">
			<li class="btn_arrow on"><a href="<?php bloginfo('url'); ?>/privacy/">個人情報保護</a></li>
		</ul>
	<?php } ?>
		<ul class="side_bunnar">
			<li><a href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/side/btn_contact_off.jpg" alt=""></a></li>
			<li><a href="<?php bloginfo('url'); ?>/outline/access/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/side/btn_access_off.jpg" alt=""></a></li>
		</ul>
	</section>
