$(function(){
	//発火
	function startObj() {
		$(window).load(function(){
			fadeObj();
		});
	}
	startObj();

	function fadeObj(){
		var elm = $('#main_v > li');
		var delaySpeed = 100; // 1つ目が表示されてから2つ目が表示されるまでの時間　1000=1秒
		var fadeSpeed = 500; // フェードインしてくるスピード(何秒かけて表示するか)　1000=1秒
		elm.delay(100).each(function(i){
			// 順番にフェードイン
			$(this).delay(i*(delaySpeed)).css({display:'block',opacity:'0'}).animate({opacity:'1'},fadeSpeed);
		});
		setTimeout(function(){
			mainVisual();
		},1600);
	}
	//メインビジュアルの動き
	function mainVisual(){
		var Speed = 300; //アニメーションのスピード
		$('#main_v > li').hover(function(){ //マウスオーバ時処理
			$(this).addClass("selected"); //オーバされた要素にクラス付与
			$(this).stop(true,false).animate({width:'37.5%'},Speed); //オーバされた要素の横幅指定
			$("#main_v > li").not(".selected").stop(true,false).animate({width:'31.1%'},Speed); //オーバされてない要素の横幅指定
		}, function(){ //マウスが離れた時処理
			$(this).removeClass("selected"); //付与されたクラスの削除
			$("#main_v > li").stop(true,false).animate({width:'33.2%'},Speed); //要素を元の横幅に指定
		});
	}

	//オーバ時メニュ表示
	$('#main_v > li').hover(function(){
		$(this).children('div').stop(true,false).fadeIn(400);
	}, function(){
		$(this).children('div').stop(true,false).fadeOut(400);
	});
	


		$(".index_content .menu_area li a").hover(function() {
		$(this).stop().animate({backgroundColor: "#2b86be",color: "#fff"}, 100);
		 },function() {
		 $(this).stop().animate({backgroundColor: "#fff",color: "#2b86be" }, 50);
		 });

		$(".index_content span a").hover(function() {
		$(this).stop().animate({backgroundColor: "#2b86be",color: "#fff"}, 100);
		 },function() {
		 $(this).stop().animate({backgroundColor: "#fff",color: "#2b86be" }, 50);
		 });


});
