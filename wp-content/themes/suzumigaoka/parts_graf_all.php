<?php
/**
 * 全ての科の表
 */?>
				<ul class="kakuka_tab cf">
					<li class="select">内　科</li>
					<li>整形外科</li>
					<li>神経内科</li>
					<li>外　科</li>
					<li>リハビリテーション科</li>
				</ul>
				<ul class="area_content">
					<li>
					<?php query_posts('p=7&post_type=doctor'); //
								if (have_posts()) : while (have_posts()) : the_post(); 	?>
					<h5>内科</h5>
					<table>
						<tr>
							<th>曜日</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日・祝</th>
						</tr>
						<tr>
							<td>午前<br>9:00〜13:00</td>
							<td><?php $val =  scf::get('am-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
						<tr>
							<td>午後<br>14:00〜18:00</td>
							<td><?php $val =  scf::get('pm-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
					</table>
					<p>午前の受付時間 8:30〜13:00<br>午後の受付時間 13:00〜18:00</p>
					<?php endwhile; endif; wp_reset_query(); ?>
					</li>
					<li class="hide">
					<?php query_posts('p=6&post_type=doctor'); //整形外科
								if (have_posts()) : while (have_posts()) : the_post(); 	?>
					<h5>整形外科</h5>
					<table>
						<tr>
							<th>曜日</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日・祝</th>
						</tr>
						<tr>
							<td>午前<br>9:00〜13:00</td>
							<td><?php $val =  scf::get('am-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
						<tr>
							<td>午後<br>14:00〜18:00</td>
							<td><?php $val =  scf::get('pm-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
					</table>
					<p>午前の受付時間 8:30〜13:00<br>午後の受付時間 13:00〜18:00</p>
					<?php endwhile; endif; wp_reset_query(); ?>
					</li>
					<li class="hide">
					<?php query_posts('p=8&post_type=doctor'); //神経内科
								if (have_posts()) : while (have_posts()) : the_post(); 	?>
					<h5>神経内科</h5>
					<table>
						<tr>
							<th>曜日</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日・祝</th>
						</tr>
						<tr>
							<td>午前<br>9:00〜13:00</td>
							<td><?php $val =  scf::get('am-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
						<tr>
							<td>午後<br>14:00〜18:00</td>
							<td><?php $val =  scf::get('pm-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
					</table>
					<p>午前の受付時間 8:30〜13:00<br>午後の受付時間 13:00〜18:00</p>
					<?php endwhile; endif; wp_reset_query(); ?>
					</li>
					<li class="hide">
					<?php query_posts('p=9&post_type=doctor'); //外科
								if (have_posts()) : while (have_posts()) : the_post(); 	?>
					<h5>外科</h5>
					<table>
						<tr>
							<th>曜日</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日・祝</th>
						</tr>
						<tr>
							<td>午前<br>9:00〜13:00</td>
							<td><?php $val =  scf::get('am-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
						<tr>
							<td>午後<br>14:00〜18:00</td>
							<td><?php $val =  scf::get('pm-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
					</table>
					<p>午前の受付時間 8:30〜13:00(予約制)</p>
					<?php endwhile; endif; wp_reset_query(); ?>
					</li>
					<li class="hide">
					<?php query_posts('p=10&post_type=doctor'); //リハビリテーション
								if (have_posts()) : while (have_posts()) : the_post(); 	?>
					<h5>リハビリテーション科</h5>
					<table>
						<tr>
							<th>曜日</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日・祝</th>
						</tr>
						<tr>
							<td>午前<br>9:00〜13:00</td>
							<td><?php $val =  scf::get('am-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
						<tr>
							<td>午後<br>14:00〜18:00</td>
							<td><?php $val =  scf::get('pm-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
					</table>
					<p>午前の受付時間 8:30〜13:00<br>午後の受付時間 13:00〜18:00</p>
					<?php endwhile; endif; wp_reset_query(); ?>
					</li>
				</ul>
				<br>
