<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="main_image">
<ul class="cf">
<li><a href="<?php bloginfo('url'); ?>">ホーム</a></li>
<li>検索結果</li>
</ul>
<h2><img src="<?php bloginfo('template_url'); ?>/common/images/title/img_search.jpg" alt="検索結果"></h2>
</div>

<div id="changeArea">
<div id="content" class="content cf">
<?php get_sidebar(); ?>
<section class="flr archive_page">
	<?php $allsearch =& new WP_Query("s=$s&posts_per_page=-1");
	$key = wp_specialchars($s, 1);
	$count = $allsearch->post_count;
	if($count!=0){
	// 検索結果を表示:該当記事あり
			echo '<p class="seach_keywords">"'.$key.'"の検索結果<span>'.$count.'件見つかりました。</span></p>';
	}
	else {
	// 検索結果を表示:該当記事なし
			echo '<p class="seach_keywords">"'.$key.'"の検索結果<span>関連する記事は見つかりませんでした</span></p>';
	}
	?>
	<ul class="news_list">
	<?php if(have_posts()): while(have_posts()): the_post();?>
		<li class="cf"><span><?php the_time('Y.m.d'); ?></span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	<?php endwhile; ?>
	<?php endif; ?>
	</ul>
	<?php wp_pagenavi(); ?>

</section>
</div>

<?php
get_footer();
