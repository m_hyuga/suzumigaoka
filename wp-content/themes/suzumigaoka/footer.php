<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 */
?>
<footer>
	<p class="btn_totop"><a href="#pagetop"><img src="<?php bloginfo('template_url'); ?>/common/images/common/btn_totop.png" alt="ページトップへ" class="over"></a></p>
	<dl>
		<dt>
			<h2>医療社団法人六豊会<br>すずみが丘病院</h2>
			<p>
			〒920-1167<br>
			石川県金沢市もりの里3丁目76番地<br>
			<a href="<?php bloginfo('url'); ?>/outline/access/">アクセスマップ</a>
			</p>
			<h3>お問い合わせ先</h3>
			<p>
			TEL：076-260-7700<br>
			FAX：076-260-7780<br>
			<a href="<?php bloginfo('url'); ?>/contact/">お問い合わせフォーム</a>
			</p>
			<h3>外来診療時間</h3>
			<p>
			午前　9:00〜13:00<br>
			午後　14:00〜18:00<br>
			休診日　日曜日・祝祭日
			</p>
		</dt>
		<dd>
			<h2>サイトマップ</h2>
			<div>
				<ul>
					<li class="btn_arrow">新着情報</li>
					<li><a href="<?php bloginfo('url'); ?>/information/">重要なお知らせ</a></li>
					<li><a href="<?php bloginfo('url'); ?>/news/">新着情報一覧</a></li>
				</ul>
				<ul>
					<li class="btn_arrow">病院のご案内</li>
					<li><a href="<?php bloginfo('url'); ?>/outline/">ごあいさつ</a></li>
					<li><a href="<?php bloginfo('url'); ?>/outline/doctor/">医師紹介</a></li>
					<li><a href="<?php bloginfo('url'); ?>/outline/rinen/">基本理念</a></li>
					<li><a href="<?php bloginfo('url'); ?>/outline/detail/">病院概要</a></li>
					<li><a href="<?php bloginfo('url'); ?>/outline/shisetsu/">施設紹介</a></li>
					<li><a href="<?php bloginfo('url'); ?>/outline/shien/">居宅介護支援事業所</a></li>
					<li><a href="<?php bloginfo('url'); ?>/outline/access/">アクセス</a></li>
				</ul>
			</div>
			<div>
				<ul>
					<li class="btn_arrow">外来診療のご案内</li>
					<li><a href="<?php bloginfo('url'); ?>/outpatient/">診療時間・受付について</a></li>
					<li><a href="<?php bloginfo('url'); ?>/outpatient/medical/">診療科目のご紹介</a></li>
					<li class="btn_under"><a href="<?php bloginfo('url'); ?>/outpatient/medical/naika/">内科</a></li>
					<li class="btn_under"><a href="<?php bloginfo('url'); ?>/outpatient/medical/seikei/">整形外科</a></li>
					<li class="btn_under"><a href="<?php bloginfo('url'); ?>/outpatient/medical/shinkei/">神経内科</a></li>
					<li class="btn_under"><a href="<?php bloginfo('url'); ?>/outpatient/medical/geka/">外科</a></li>
					<li class="btn_under"><a href="<?php bloginfo('url'); ?>/outpatient/medical/rihabiri/">リハビリテーション科</a></li>
					<li><a href="<?php bloginfo('url'); ?>/outpatient/tsusho/">通所リハビリテーション</a></li>
				</ul>
				<ul>
					<li class="btn_arrow">入院のご案内</li>
					<li><a href="<?php bloginfo('url'); ?>/guide/">入院のお手続きについて</a></li>
					<li><a href="<?php bloginfo('url'); ?>/guide/meeting/">お見舞い・面会について</a></li>
				</ul>
			</div>
			<div class="map_last">
				<ul>
					<li class="btn_arrow"><a href="<?php bloginfo('url'); ?>/recruitment/">採用情報</a></li>
					<li class="btn_arrow"><a href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></li>
					<li class="btn_arrow"><a href="<?php bloginfo('url'); ?>/privacy/">個人情報保護方針</a></li>
					<li class="btn_arrow"><a href="<?php bloginfo('url'); ?>/sitemap/">サイトマップ</a></li>
				</ul>
			</div>
		</dd>
	</dl>
	<p class="copy">Copyright &copy; 医療法人社団六豊会 すずみが丘病院. All Rights Reserved.</p>
</footer>
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>
