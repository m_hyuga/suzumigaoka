<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<div id="main_image">
<ul class="cf">
<li><a href="<?php bloginfo('url'); ?>">ホーム</a></li>
<li><?php if ( is_singular('information')) :?><a href="<?php bloginfo('url'); ?>/information/">大切なお知らせ</a><?php else: ?><a href="<?php bloginfo('url'); ?>/news/">新着情報</a><?php endif; ?></li>
<li><?php if(mb_strlen($post->post_title)>20) { $title= mb_substr($post->post_title,0,20) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></li>
</ul>
<h2><img src="<?php bloginfo('template_url'); ?>/common/images/title/<?php if ( is_singular('information')) :?>img_taisetsu.jpg" alt="大切なお知らせ"<?php else: ?>img_news.jpg" alt="新着情報"<?php endif; ?>></h2>
</div>

<div id="changeArea">
<div id="content" class="content cf">
<?php get_sidebar(); ?>
<section class="flr single_page">
	<div class="news_datail cf">
	<h4 class="single_title cf"><strong><?php the_title(); ?></strong><span><?php the_time('Y.m.d'); ?></span></h4>
		<div class="mceContentBody">
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/editor-style.css" />
		<?php the_content(); ?>
		</div>
	</div>
	<?php endwhile; endif; ?>
	
</section>
</div>
<?php
get_footer();
