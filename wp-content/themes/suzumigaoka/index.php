<?php
/**
 * The main template file
 *
 */

get_header(); ?>
	<!-- ここからメインビジュアル -->
	<ul id="main_v">
		<li class="sl_01">
			<div>
				<a href="<?php bloginfo('url'); ?>/outpatient/">診療時間・受付について</a>
				<a href="<?php bloginfo('url'); ?>/outpatient/medical/">診療科目のご紹介</a>
			</div>
		</li>
		<li class="sl_02">
			<div>
				<a href="<?php bloginfo('url'); ?>/guide/">入院のお手続きについて</a>
				<a href="<?php bloginfo('url'); ?>/guide/meeting/">お見舞い・面会について</a>
			</div>
		</li>
		<li class="sl_03">
			<div>
				<a href="<?php bloginfo('url'); ?>/outline/">ごあいさつ</a>
				<a href="<?php bloginfo('url'); ?>/outline/doctor/">医師紹介</a>
				<a href="<?php bloginfo('url'); ?>/outline/rinen/">基本理念</a>
				<a href="<?php bloginfo('url'); ?>/outline/detail/">病院概要</a>
				<a href="<?php bloginfo('url'); ?>/outline/access/">アクセス</a>
			</div>
		</li>
	</ul>
	<!-- ここまでメインビジュアル -->

	<div id="changeArea">
	<div id="content" class="index_content cf">
		<section class="fll">
			<div class="menu_area">
			<ul>
				<li><a href="<?php bloginfo('url'); ?>/outpatient/medical/naika/"><span>&gt;</span>内　科</a></li>
				<li><a href="<?php bloginfo('url'); ?>/outpatient/medical/seikei/"><span>&gt;</span>整形外科</a></li>
				<li><a href="<?php bloginfo('url'); ?>/outpatient/medical/shinkei/"><span>&gt;</span>神経内科</a></li>
				<li><a href="<?php bloginfo('url'); ?>/outpatient/medical/geka/"><span>&gt;</span>外　科</a></li>
				<li class="menu_riha"><a href="<?php bloginfo('url'); ?>/outpatient/medical/rihabiri/"><span>&gt;</span>リハビリ<br>テーション科</a></li>
			</ul>
			</div>
		
			<div class="info_area">
				<h2>大切なお知らせ<span><a href="<?php bloginfo('url'); ?>/information/">&gt;&nbsp;一覧を見る</a></span></h2>
				<dl>
					<?php query_posts( Array(
							'post_type' => array('information'),
							'showposts' => 3
							));
						if (have_posts()) : while (have_posts()) : the_post(); ?>
					<dt><?php the_time('Y.m.d'); ?></dt>
					<dd><a href="<?php the_permalink(); ?>"><?php if(mb_strlen($post->post_title)>30) { $title= mb_substr($post->post_title,0,30) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></a></dd>
					<?php endwhile; ?>
					<?php else : ?>
					新着のお知らせはありません。
					<?php endif; ?>
				</dl>
			</div>
			<div class="schedule_area">
				<h2>各科の担当医<span><a href="<?php bloginfo('url'); ?>/outpatient/">&gt;&nbsp;各科のスケジュール</a></span></h2>
				<ul class="kakuka_tab cf">
					<li class="select">内　科</li>
					<li>整形外科</li>
					<li>神経内科</li>
					<li>外　科</li>
					<li>リハビリテーション科</li>
				</ul>
				<ul class="area_content">
					<li>
					<?php query_posts('p=7&post_type=doctor'); //内　科
								if (have_posts()) : while (have_posts()) : the_post(); 	?>
					<table>
						<tr>
							<th>曜日</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日・祝</th>
						</tr>
						<tr>
							<td>9:00〜13:00</td>
							<td><?php $val =  scf::get('am-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
						<tr>
							<td>14:00〜18:00</td>
							<td><?php $val =  scf::get('pm-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
					</table>
					<?php endwhile; endif; ?>
					</li>
					<li class="hide">
					<?php query_posts('p=6&post_type=doctor'); //整形外科
								if (have_posts()) : while (have_posts()) : the_post(); 	?>
					<table>
						<tr>
							<th>曜日</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日・祝</th>
						</tr>
						<tr>
							<td>9:00〜12:30</td>
							<td><?php $val =  scf::get('am-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
						<tr>
							<td>14:00〜18:00</td>
							<td><?php $val =  scf::get('pm-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
					</table>
					<?php endwhile; endif; ?>
					</li>
					<li class="hide">
					<?php query_posts('p=8&post_type=doctor'); //整形外科
								if (have_posts()) : while (have_posts()) : the_post(); 	?>
					<table>
						<tr>
							<th>曜日</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日・祝</th>
						</tr>
						<tr>
							<td>9:00〜12:30</td>
							<td><?php $val =  scf::get('am-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
						<tr>
							<td>14:00〜18:00</td>
							<td><?php $val =  scf::get('pm-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
					</table>
					<?php endwhile; endif; ?>
					</li>
					<li class="hide">
					<?php query_posts('p=9&post_type=doctor'); //整形外科
								if (have_posts()) : while (have_posts()) : the_post(); 	?>
					<table>
						<tr>
							<th>曜日</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日・祝</th>
						</tr>
						<tr>
							<td>9:00〜12:30</td>
							<td><?php $val =  scf::get('am-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
						<tr>
							<td>14:00〜18:00</td>
							<td><?php $val =  scf::get('pm-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
					</table>
					<?php endwhile; endif; ?>
					</li>
					<li class="hide">
					<?php query_posts('p=10&post_type=doctor'); //整形外科
								if (have_posts()) : while (have_posts()) : the_post(); 	?>
					<table>
						<tr>
							<th>曜日</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日・祝</th>
						</tr>
						<tr>
							<td>9:00〜12:30</td>
							<td><?php $val =  scf::get('am-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('am-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
						<tr>
							<td>14:00〜18:00</td>
							<td><?php $val =  scf::get('pm-mon'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-tu'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-wed'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-thur'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-fri'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sat'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
							<td><?php $val =  scf::get('pm-sun'); if (empty($val)) {echo '';} else {	echo $val;	} ?></td>
						</tr>
					</table>
					<?php endwhile; endif; ?>
					</li>
				</ul>
			</div>
			<div class="news_area">
				<h2>新着情報<span><a href="<?php bloginfo('url'); ?>/news/">&gt;&nbsp;一覧を見る</a></span></h2>
					<?php query_posts( Array('showposts' => 3));
					if (have_posts()) : while (have_posts()) : the_post(); ?>
					<dt><?php the_time('Y.m.d'); ?></dt>
					<dd><a href="<?php the_permalink(); ?>"><?php if(mb_strlen($post->post_title)>30) { $title= mb_substr($post->post_title,0,30) ; echo $title. ･･･ ;} else {echo $post->post_title;}?></a></dd>
					<?php endwhile; ?>
					<?php else : ?>
					新着のお知らせはありません。
					<?php endif; ?>
			</div>
		</section>
		<section class="flr">
			<dl>
				<dt>診療時間のご案内</dt>
				<dd>
					<h3>診療時間</h3>
					<p>9:00〜13:00</p>
					<p class="txt_min"> (受付 8:30〜13:00)</p>
					<p>14:00〜18:00</p>
					<p class="txt_min">(受付 13:00〜18:00)</p>
					<h3>休診日</h3>
					<p>日曜日・祝祭日</p>
				</dd>
			</dl>
			<ul>
				<li><a href="<?php bloginfo('url'); ?>/outpatient/medical/"><img src="<?php bloginfo('template_url'); ?>/common/images/index/btn_guide_off.jpg" alt="診療科目のご案内"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/outpatient/tsusho/"><img src="<?php bloginfo('template_url'); ?>/common/images/index/btn_rihabiri_off.jpg" alt="通所リハビリ"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/outline/shien/"><img src="<?php bloginfo('template_url'); ?>/common/images/index/btn_shien_off.jpg" alt="居宅介護支援事業所"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/outline/access/"><img src="<?php bloginfo('template_url'); ?>/common/images/index/btn_access_off.jpg" alt="交通アクセス"></a></li>
				<li><a href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/common/images/index/btn_contact_off.jpg" alt="お問い合わせ"></a></li>
			</ul>
		</section>
	</div>
<?php get_footer(); ?>
